# STARLORD (better name pending)

tech stack - flask + SQLalchemy + SQLlite + (pending db admin)

requirements:
 - flask
 - SQLalchemy
 - Flask-login
 - Flask-SQLalchemy

## Frontend

Vue 3 with Vite as dev tool.

```
npm install
npm run lint
npm run dev
```

To build the project, run `npm run build`.

To change the output directory, change `outDir` in `vite.config.ts`:
```
    build: {
        outDir: "../dist",
        emptyOutDir: true,
    },
```
