from website import create_app

if __name__ == '__main__':
    app = create_app()
    app.run(debug=True)
    
    from . import db
    db.init_app(app)


