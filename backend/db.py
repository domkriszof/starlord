import click
from flask import current_app, g
from flask.cli import with_appcontext

from . import db
from flask_login import UserMixin
from sqlalchemy.sql import func

class Faction(db.Model):
    """zaloz frakci [barva, IP, DP, (sektory), (hraci), (projekty (investice IP/DP)) """
    id = db.Column(db.Integer, primary_key=True)
    IP_avalible = db.Column(db.Integer)
    DP_avalible = db.Column(db.Integer)
    players = db.relationship('Player')
    teritory = db.relationship('Sector')


class Player(db.Model, UserMixin):
    """zaloz hrace [login, password_hash, ingame_name, >frakce, %IP, %DP, ]"""
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(80), unique=True, nullable=False)
    password = db.Column(db.String(80)) # TODO hash this
    name = db.Column(db.String(80), unique=True, nullable=False)
    IP_access = db.Column(db.Integer)
    DP_access = db.Column(db.Integer)
    faction_id = db.Column(db.Integer, db.ForeingKey('Faction.id'))


class Product(db.Model):
    """"zaloz produky [name, sila/cennost?]"""
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True)
    rarity = db.Column(db.Integer)


class Sector(db.Model):
    """zaloz sektor [name, (frakce komu patri), politicka sila, industrialni sila, >produkuje, >projekty?, >politicke_rozlozeni]"""
    id = db.Column(db.Integer, primary_key=True)
    political_gain = db.Column(db.Integer)
    industrial_gain = db.Column(db.Integer)
    faction_id = db.Column(db.Integer, db.ForeingKey('Faction.id'))


class Development(db.Model):
    """ prunikova tabulka projekty(frakce, sektor, cost, vysledek ) """
    id = db.Column(db.Integer, primary_key=True)
    sector_id = db.Column(db.Integer)
    already_spent = db.Column(db.Integer)
    price_DP = db.Column(db.Integer)
    price_IP = db.Column(db.Integer)
    effect = db.Column(db.Integer)
    date = db.Column(db.DateTime(timezone=True), default=func.now())


class Production(db.Model):
    """ prunikova tabulka produkce(sektor, frakce, produkt, objem) """
    id = db.Column(db.Integer, primary_key=True)
    faction_id = db.Column(db.Integer, db.ForeingKey('Faction.id'))
    product_id = db.Column(db.Integer)
    volume_created = db.Column(db.Integer)


class Goverment(db.Model):
    """ prunikova tabulka politicke rozlozeni(frakce, sektor, sila) """
    id = db.Column(db.Integer, primary_key=True)
    sector_id = db.Column(db.Integer)
    faction_id = db.Column(db.Integer, db.ForeingKey('Faction.id'))
    power = db.Column(db.Integer)


class Trade(db.Model):
    """ prunikova tabulka trade (odkud, komu, kolik, produkt) """
    id = db.Column(db.Integer, primary_key=True)
    from_faction_id = db.Column(db.Integer, db.ForeingKey('Faction.id'))
    to_faction_id = db.Column(db.Integer, db.ForeingKey('Faction.id'))
    product_id = db.Column(db.Integer)
    volume = db.Column(db.Integer)


def get_db():
    if 'db' not in g:
        g.db = db.connect(
            current_app.config['DATABASE'],
            detect_types= db.PARSE_DECLTYPES
        )
        g.db.row_factory = db.Row

    return g.db


def close_db(e=None):
    db = g.pop('db', None)

    if db is not None:
        db.close()


@click.command('init-db')
@with_appcontext
def init_db_command():
    """Clear the existing data and create new tables."""

    populate_db()
    click.echo('Initialized the database.')


def populate_db():
    pass
