import flask
from flask import Blueprint, render_template, request, flash, redirect
from flask_login import login_user, logout_user, login_required
from .db import Player
from . import db
from werkzeug.security import generate_password_hash, check_password_hash
auth = Blueprint('auth', __name__)


@auth.route('/sign-out', methods=['GET', 'POST'])
@login_required
def sign_out_screen():
    logout_user()
    data = request.form
    return render_template("sign_up.html")


@auth.route('/login', methods=['GET', 'POST'])
def login_screen():

    if request.method == "POST":
        email = request.form.get("email")
        password = request.form.get("password")
        user = Player.query.filter_by(email=email).first()
        if user:
            if check_password_hash(user.password, password):
                flash("Logged in", category="success")
                login_user(user, remember=True)
                return redirect(flask.url_for("maps.base"))
            else:
                flash("Incorrect password or bad mail", category="error")
        else:
            flash("Incorrect password or bad mail", category="error")

    data = request.form
    return render_template("login.html")


@auth.route('/sign-in', methods=['GET', 'POST'])
def sign_up_screen():
    if request.method == 'POST':
        email = request.form.get('email')
        nickname = request.form.get('nickname')
        password = request.form.get('password')
        password_again = request.form.get('password_again')

        user = Player.query.filter_by(email=email).first()
        if user:
            flash("email must be grater than 4 characters, category='error'")

        elif len(email) < 4:
            flash("email must be grater than 4 characters, category='error'")

        elif len(nickname) < 2:
            flash("nickname must be grater than 2 characters, category='error'")

        elif password != password_again:
            flash("password don\'t match, category='error'")

        elif len(password) < 7:
            flash("password must be at least 8 characters, category='error'")
        else:
            #todo check and fix new user
            new_user = Player(email=email, name=nickname,password=generate_password_hash(password, method="sha512"))
            db.session.add(new_user)
            db.session.commit()
            login_user(user, remember=True)
            flash("account created, category='success'")
            return redirect(flask.url_for("auth.login_screen"))

    return render_template("sign_out.html")

