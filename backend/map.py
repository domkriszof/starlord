from flask import Blueprint, render_template
from flask_login import login_user, login_required, logout_user, current_user

maps = Blueprint('map', __name__)


##TODO add login requred everywhere
# TODO also make funcional
@maps.route("/")
@login_required
def base():
    return render_template("base.html", current_menu="base")


@maps.route('/infrastructure', methods=['GET', 'POST'])
def infrastructure_screen():
    # musi zobrazit aktualni status surovin + postih as sum (proklik na trade)
    # zobrazit produkci jednotlivych sektoru + moznost zvysit jejich produci (plus dalsi development projects)
    # produkce lodi, zefektivnovani vyrobnich kapacit
    return render_template("game_screens/infrastructure.html")


@maps.route('/diplomacy', methods=['GET', 'POST'])
def diplomacy_screen():
    # musi bejt schopnej naklikavat armadu zde
    # musi bejt schpen utarcet DP na konkretni sektrory/projekty/frakce

    return render_template("game_screens/diplomacy.html")


@maps.route('/trade', methods=['GET', 'POST'])
def trade_screen():
    # musi zobrazit chybjejici/prebyvajici suroviny
    # mapa - jednoduse nakliknutelne frakce a u nich +- co davaji berou - s proklikem na pecifickou frakci (focus na ni)
    return render_template("game_screens/trade.html")

@maps.route('/profile', methods=['GET', 'POST'])
def profile_screen():
    return render_template("game_screens/pofile.html")


def do_tick():
    # utrati vsechny dostupne resources (zacne posledne nakliknutejma?)
    # DP projekty - odecte body a pokud >=100% tak je aplikuje uz toto kolo
    # IP projekty - odecte body a pokud >=100% tak je aplikuje uz toto kolo
    # prebytek se prevede na penize?

    # valka - posune vsechny armady a vyhodnoti boj
    # prepocita sektory - political power / military a podle toho se zmeni vlastnictvi

    # udela avalible resource sum na nasledujici kolo
    # vezme a secte vsechny sektory a rozdeli diplomaticke body na pristi kolo
    # udela vsechny trade posuny
    # vyhodnoti produkci kolik ceho se ziskalo a vyrobilo
    # chybjejici resources = postih

    # done

    pass


def map_display():
    # default: politicke zobrazeni + armady
    # trade: pri nakliknuti zdroje suroviny + sipky s existujicimi trade routes
    # industry: vyvinutost regionu?
    # diplomacy: politicke zobrazeni + diplomaticke body + ambasady

    return None
