import { ref, computed } from "vue";
import { defineStore } from "pinia";

import type { MapPoint } from "../models/MapPoint";

export const useMapStore = defineStore("map", () => {
    const state = ref<MapPoint[]>([
        {
            id: 1,
            x: 20,
            y: 500,
            faction: "evil",
            color: "red",
        },
        {
            id: 2,
            x: 500,
            y: 20,
            faction: "badass",
            color: "blue",
        },
        {
            id: 3,
            x: 100,
            y: 100,
            faction: "good",
            color: "green",
        },
        {
            id: 4,
            x: 600,
            y: 600,
            faction: "chaotic",
            color: "yellow",
        },
    ]);

    return { state };
});
