export interface MapPoint {
    id: number;
    x: number;
    y: number;
    faction: string;
    color: string;
}
